import datetime, json, requests, os, sys, logging

#Logger
fileHandler = logging.FileHandler(filename='/var/log/alarmmonitor.log')
stdoutHandler = logging.StreamHandler(sys.stdout)
handlers = [fileHandler, stdoutHandler]
logging.basicConfig(handlers=handlers, level=logging.INFO, format='[%(asctime)s] - %(levelname)s - %(message)s')
logging.info('Starting script')

#Params
url = 'https://api.blaulichtsms.net/blaulicht'
loginPath = '/api/alarm/v1/dashboard/login'
alarmPath = '/api/alarm/v1/dashboard/'

customerId = ''
username = ''
password = ''

hdmiDevices = ['0']
sessionId = None
hasAlarm = False

time = datetime.datetime.utcnow()
alarmDuration = datetime.timedelta(hours=2)

#Login
try:
  headers = {'Content-Type': 'application/json', 'Host': '', 'Content-Length': '100'}
  body = {
    'username': username,
    'password': password,
    'customerId': customerId
  }

  loginResult = requests.post(url + loginPath, json = body)
  sessionId = loginResult.json()['sessionId']
  if sessionId == None:
    raise Exception(loginResult.json()['error'])

  logging.info('Login Success')
except Exception as e:
  logging.error('Failed to login: ' + e.args[0])
  sys.exit()
except:
  logging.error('Failed to login (unknown reason)')
  sys.exit()

#Alarms
try:
  alarmResult = requests.get(url + alarmPath + sessionId)
  alarms = alarmResult.json()['alarms']
  if alarms == None:
    raise Exception(loginResult.json()['error'])  

  if len(alarms) == 0:
    logging.info('No Alarms')
  for alarm in alarms:
   alarmTime = datetime.datetime.strptime(alarm["alarmDate"], "%Y-%m-%dT%H:%M:%S.%fZ")
   if alarmTime > time - alarmDuration:
     hasAlarm = True
     logging.info('Alarm: ' + alarm['alarmId'] + 'is active')
   else:
     logging.info('Alarm: ' + alarm['alarmId'] + 'is inactive')
except Exception as e:
  logging.error('Cannot get Alarms:' + e.args[0])
  sys.exit()
except:
  logging.error('Cannot get Alarms (unknown reason)')
  sys.exit()

#CEC
if hasAlarm:
  for device in hdmiDevices:
    logging.info('Turning on device ' + device + ' via CEC')
    os.system('echo "on ' + device + '" | cec-client -s -d 1')

logging.info('Finnished script');


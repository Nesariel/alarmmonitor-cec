# BlaulichtSMS Alarmmonitor HDMI CEC Controller



## Getting started
```
git clone https://gitlab.com/Nesariel/alarmmonitor-cec.git
```


## Installing dependencies

- [ ] CEC Utils is the library to send cec commands via hdmi
- [ ] Unclutter for hiding the mouse cursor after 30 seconds (if u want to use the same rasp pi to display the alarmmonitor in browser)

```
apt-get update && sudo apt-get upgrade
apt install cec-utils
apt install unclutter
```

## Add Alarmmonitor credentials

in alarmmonitor.py
```
customerId = ''
username = ''
password = ''
```

## Register as a service
```
ln -s /home/pi/alarmmonitor.service /etc/systemd/system/
systemctl enable alarmmonitor
systemctl start alarmmonitor
```

## Create and make the logfile accessable
```
/var/log/alarmmonitor.log
chmod 777 /var/log/alarmmonitor.log
```

